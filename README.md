## SPEC Macro Motor Flyscanning
### Macro Utlization
`fly_macro` is the main macro macro which will allow the user to flyscan a SPEC configured macro motor. Users are required to provide 5 arguments to `fly_macro` to perform the flyscan:
- **mne** - macro motor mnemonic as defined in the SPEC config file.
- **start position** - starting position for the flyscan in millimeters or degrees.
- **end position** - final position for the flyscan in millimeters or degrees.
- **intervals** - determines how many pieces the flyscan will be split into for data collection. this can be configured to be either the number of bins per flyscan or the width of each bin.
- **bin time** - can be configured as the time for each bin to be completed or the total time for the flyscan. 

A general overview of its use is

    > Calculate real motor positions using the macro motors innate _calc function.

    > Calculate the galil electronic gear ratio for the real motors for the flyscan.

    > Initialize the flyscan with the user defined parameters. 

    > Synchronize the gearing via chained defs pre and post scan.

### Sanity/Safety Checks

- All real motors must be on the same controller in order to perform the flyscan or the program will be exited.
- The first argument for `fly_macro` must be a macro motor mnemonic and of the controller type **MAC_MOT** in the SPEC configuration file.
-  The `flyn_check_lim` macro will determine if a motor is approaching an unsafe limit and exit the program if needed. 

## Build Progress
SPEC utilities to execute flyscanning with macro motors have been developed and tested, and are now ready for beamline deployment.